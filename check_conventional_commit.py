#!/usr/bin/env python3

import sys
import re

CONVCOMM_REGEX = re.compile(r"^(feat|fix|chore)!?(\(.*\))?: ")

good = True

for line in sys.stdin.readlines():
    if CONVCOMM_REGEX.match(line.strip().lower()) is None:
        good = False
        print(
            f"Error: commit not adhering to conventional commit\n    {line.strip()}")

exit(0 if good else 1)
