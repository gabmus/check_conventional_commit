# Check Conventional Commits

Quick and dirty script to check if a git log contains or not messages adhering to the [Conventional Commits specification](https://www.conventionalcommits.org).

# Usage

```bash
git log --pretty=format:%s | ./check_conventional_commit.py
```

Or if you only want to check the delta from the current branch to the main one (useful in pipelines):

```bash
git log main..HEAD --pretty=format:%s | ./check_conventional_commit.py
```
